package com.worldpay.andrei.offers.controller;

import com.worldpay.andrei.offers.exceptions.OfferNotFoundException;
import com.worldpay.andrei.offers.model.Offer;
import com.worldpay.andrei.offers.model.Product;
import com.worldpay.andrei.offers.service.OfferService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class OfferRestControllerTest {
    private static final String DESCRIPTION_ONE = "Free next day delivery on orders placed before 5pm";
    private static final String DESCRIPTION_TWO = "Buy 5 for the price of 4!";
    @Mock
    private OfferService offerService;
    private MockMvc mockMvc;
    @InjectMocks
    private OfferRestController offerRestController;
    private Offer offer;
    private Offer offer2;
    private List offers;
    private Product product;

    @Before
    public void setUp() {
        reset(offerService);
        mockMvc = MockMvcBuilders.standaloneSetup(offerRestController).build();
        product = mock(Product.class);
        offer = new Offer(product, DESCRIPTION_ONE, new BigDecimal(10), "GBP");
        offer2 = new Offer(product, DESCRIPTION_TWO, new BigDecimal(40), "GBP");
        offers = new ArrayList<>();
        offers.add(offer);
        offers.add(offer2);
        when(offerService.getAllOffers(1L)).thenReturn(offers);
    }

    @Test
    public void getOffersReturnsAllOffers() throws Exception {
        mockMvc.perform(get("/products/1/offers"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(0))
                .andExpect(jsonPath("$[0].description").value(DESCRIPTION_ONE))
                .andExpect(jsonPath("$[0].price").value(10))
                .andExpect(jsonPath("$[0].priceCurrency").value("GBP"))
                .andExpect(jsonPath("$[1].id").value(0))
                .andExpect(jsonPath("$[1].description").value(DESCRIPTION_TWO))
                .andExpect(jsonPath("$[1].price").value(40))
                .andExpect(jsonPath("$[1].priceCurrency").value("GBP"));
        verify(offerService).getAllOffers(1L);
    }

    @Test
    public void getOfferReturnsRequestedoffer() throws Exception {
        when(offerService.getOffer(1,1)).thenReturn(offer);
        mockMvc.perform(get("/products/1/offers/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(0))
                .andExpect(jsonPath("$.description").value(DESCRIPTION_ONE))
                .andExpect(jsonPath("$.price").value(10))
                .andExpect(jsonPath("$.priceCurrency").value("GBP"));

        verify(offerService).getOffer(1, 1);
    }
    @Test
    public void postOfferReturnsNewlyCreatedOffer() throws Exception {
        when(offerService.addOffer(1,DESCRIPTION_ONE, "10.00", "GBP")).thenReturn(offer);

        mockMvc.perform(post("/products/1/offers")
                .param("description", DESCRIPTION_ONE)
                .param("priceString","10.00")
                .param("priceCurrency", "GBP"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(0))
                .andExpect(jsonPath("$.description").value(DESCRIPTION_ONE))
                .andExpect(jsonPath("$.price").value(10))
                .andExpect(jsonPath("$.priceCurrency").value("GBP"));
    }

    @Test
    public void postOfferReturnsReturns400IfMissingDescriptionParameter() throws Exception {
        mockMvc.perform(post("/products/1/offers")
                .param("price","10")
                .param("priceCurrency", "GBP"))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void postOfferReturnsReturns400IfMissingPriceParameter() throws Exception {
        mockMvc.perform(post("/products/1/offers")
                .param("description", DESCRIPTION_ONE)
                .param("priceCurrency", "GBP"))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void postOfferReturnsReturns400IfMissingCurrencyParameter() throws Exception {
        mockMvc.perform(post("/products/1/offers")
                .param("description", DESCRIPTION_ONE)
                .param("price","10"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getOfferReturns404WhenOfferNotFound() throws Exception {
        when(offerService.getOffer(1, 3)).thenThrow(new OfferNotFoundException(1L, 3L));
        mockMvc.perform(get("/products/1/offers/3"))
                .andExpect(status().isNotFound());
        verify(offerService).getOffer(1, 3);
    }

    @Test
    public void getOfferReturns404WhenOfferIdNotAssociatedWithProductIdPassedIn() throws Exception {
        when(offerService.getOffer(1, 3)).thenThrow(new OfferNotFoundException(1L, 3L));
        mockMvc.perform(get("/products/1/offers/3"))
                .andExpect(status().isNotFound());
        verify(offerService).getOffer(1, 3);
    }

}