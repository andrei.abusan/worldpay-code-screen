package com.worldpay.andrei.offers.controller;

import com.worldpay.andrei.offers.exceptions.ProductNotFoundException;
import com.worldpay.andrei.offers.model.Product;
import com.worldpay.andrei.offers.service.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class ProductRestControllerTest {
    private static final String DESCRIPTION_ONE = "Russian solution to space writing";
    private static final String PRODUCT_NAME_ONE = "pencil";
    private static final String PRODUCT_NAME_TWO = "space pen";
    private static final String DESCRIPTION_TWO = "American solution to space writing";
    private MockMvc mockMvc;

    @Mock
    private ProductService productService;
    @InjectMocks
    private ProductRestController productRestController;
    private Product product;
    private Product product2;
    private List<Product> products;

    @Before
    public void setUp() {
        reset(productService);
        this.mockMvc = MockMvcBuilders.standaloneSetup(productRestController).build();
        product = new Product(PRODUCT_NAME_ONE, DESCRIPTION_ONE);
        product2 = new Product(PRODUCT_NAME_TWO, DESCRIPTION_TWO);
        products = new ArrayList<>();
        products.add(product);
        products.add(product2);
        when(productService.getAllProducts()).thenReturn(products);
    }

    @Test
    public void getProductsReturnsAllProducts() throws Exception {

        mockMvc.perform(get("/products"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(0))
                .andExpect(jsonPath("$[0].productName").value(PRODUCT_NAME_ONE))
                .andExpect(jsonPath("$[0].description").value(DESCRIPTION_ONE))
                .andExpect(jsonPath("$[1].id").value(0))
                .andExpect(jsonPath("$[1].productName").value(PRODUCT_NAME_TWO))
                .andExpect(jsonPath("$[1].description").value(DESCRIPTION_TWO));
        verify(productService).getAllProducts();
    }

    @Test
    public void getProductReturnsRequestedProduct() throws Exception {
        when(productService.getProduct(1)).thenReturn(product);
        mockMvc.perform(get("/products/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(0))
                .andExpect(jsonPath("$.productName").value(PRODUCT_NAME_ONE))
                .andExpect(jsonPath("$.description").value(DESCRIPTION_ONE));
        verify(productService).getProduct(1);
    }
    @Test
    public void getProductReturns404WhenNoProductFound() throws Exception {
        when(productService.getProduct(3)).thenThrow(new ProductNotFoundException(3L));
        mockMvc.perform(get("/products/3"))
                .andExpect(status().isNotFound());
        verify(productService).getProduct(3);
    }
    @Test
    public void postProductReturns405() throws Exception {
        mockMvc.perform(post("/products/3"))
                .andExpect(status().isMethodNotAllowed());
    }
    @Test
    public void postProductsReturns405() throws Exception {
        mockMvc.perform(post("/products"))
                .andExpect(status().isMethodNotAllowed());
    }

}