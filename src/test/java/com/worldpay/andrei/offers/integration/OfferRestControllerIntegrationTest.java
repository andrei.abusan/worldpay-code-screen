package com.worldpay.andrei.offers.integration;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OfferRestControllerIntegrationTest {
    private static final String DESCRIPTION_ONE = "Free next day delivery on orders placed before 5pm";
    private static final String DESCRIPTION_TWO = "Buy 5 for the price of 4!";


    @LocalServerPort
    private int port;
    private TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Test
    public void getOffersReturnsOffers() throws Exception {
        ResponseEntity<String> responseEntity = testRestTemplate.getForEntity("http://localhost:{port}/products/1/offers",
                String.class, port);
        JSONArray jsonArray = new JSONArray(responseEntity.getBody());
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
        assertThat(jsonArray.getJSONObject(0).getInt("id"), is(1));
        assertThat(jsonArray.getJSONObject(0).getString("description"), is(DESCRIPTION_ONE));
        assertThat(jsonArray.getJSONObject(0).getString("price"), is("5.0"));
        assertThat(jsonArray.getJSONObject(0).getString("priceCurrency"), is("GBP"));
        assertThat(jsonArray.getJSONObject(1).getInt("id"), is(2));
        assertThat(jsonArray.getJSONObject(1).getString("description"), is(DESCRIPTION_TWO));
        assertThat(jsonArray.getJSONObject(1).getString("price"), is("40.0"));
        assertThat(jsonArray.getJSONObject(1).getString("priceCurrency"), is("GBP"));
    }

    @Test
    public void getOfferReturnsOffer() throws Exception {
        ResponseEntity<String> responseEntity = testRestTemplate.getForEntity("http://localhost:{port}/products/1/offers/1",
                String.class, port);
        JSONObject jsonObject = new JSONObject(responseEntity.getBody());
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
        assertThat(jsonObject.getInt("id"), is(1));
        assertThat(jsonObject.getString("description"), is(DESCRIPTION_ONE));
        assertThat(jsonObject.getString("price"), is("5.0"));
        assertThat(jsonObject.getString("priceCurrency"), is("GBP"));
    }

    @Test
    public void getOfferReturns404IfNotFound(){
        ResponseEntity<String> responseEntity = testRestTemplate.getForEntity("http://localhost:{port}/products/1/offers/3",
                String.class, port);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }

    @Test
    public void postOfferReturnsNewlyCreatedOffer(){
        MultiValueMap<String, String> params= new LinkedMultiValueMap<>();
        params.add("description", DESCRIPTION_ONE);
        params.add("priceString","10.00");
        params.add("priceCurrency", "GBP");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);
        ResponseEntity<String> responseEntity = testRestTemplate.postForEntity("http://localhost:{port}/products/1/offers",
                entity, String.class,port);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.CREATED));
    }


}
