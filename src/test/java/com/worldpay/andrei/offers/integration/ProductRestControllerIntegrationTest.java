package com.worldpay.andrei.offers.integration;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductRestControllerIntegrationTest {
    private static final String DESCRIPTION_ONE = "Russian solution to space writing";
    private static final String PRODUCT_NAME_ONE = "pencil";
    private static final String PRODUCT_NAME_TWO = "space pen";
    private static final String DESCRIPTION_TWO = "American solution to space writing";


    @LocalServerPort
    private int port;
    private TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Test
    public void getProductsReturnsProducts() throws Exception {
        ResponseEntity<String> responseEntity = testRestTemplate.getForEntity("http://localhost:{port}/products",
                String.class, port);
        JSONArray jsonArray = new JSONArray(responseEntity.getBody());
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
        assertThat(jsonArray.getJSONObject(0).getInt("id"), is(1));
        assertThat(jsonArray.getJSONObject(0).getString("productName"), is(PRODUCT_NAME_ONE));
        assertThat(jsonArray.getJSONObject(0).getString("description"), is(DESCRIPTION_ONE));
        assertThat(jsonArray.getJSONObject(1).getInt("id"), is(2));
        assertThat(jsonArray.getJSONObject(1).getString("productName"), is(PRODUCT_NAME_TWO));
        assertThat(jsonArray.getJSONObject(1).getString("description"), is(DESCRIPTION_TWO));
    }

    @Test
    public void getProductReturnsProduct() throws Exception {
        ResponseEntity<String> responseEntity = testRestTemplate.getForEntity("http://localhost:{port}/products/1",
                String.class, port);
        JSONObject jsonObject = new JSONObject(responseEntity.getBody());
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.OK));
        assertThat(jsonObject.getInt("id"), is(1));
        assertThat(jsonObject.getString("productName"), is(PRODUCT_NAME_ONE));
        assertThat(jsonObject.getString("description"), is(DESCRIPTION_ONE));
    }

    @Test
    public void getProductReturns404IfNotFound(){
        ResponseEntity<String> responseEntity = testRestTemplate.getForEntity("http://localhost:{port}/products/3",
                String.class, port);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }
    @Test
    public void postProductReturns405(){
        ResponseEntity<String> responseEntity = testRestTemplate.postForEntity("http://localhost:{port}/products/1", null,
                String.class, port);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.METHOD_NOT_ALLOWED));
    }
    @Test
    public void postProductsReturns405(){
        ResponseEntity<String> responseEntity = testRestTemplate.postForEntity("http://localhost:{port}/products", null,
                String.class, port);
        assertThat(responseEntity.getStatusCode(), is(HttpStatus.METHOD_NOT_ALLOWED));
    }


}
