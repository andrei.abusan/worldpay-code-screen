package com.worldpay.andrei.offers.service;

import com.worldpay.andrei.offers.exceptions.OfferNotFoundException;
import com.worldpay.andrei.offers.model.Offer;
import com.worldpay.andrei.offers.model.Product;
import com.worldpay.andrei.offers.persistance.OfferRepository;
import com.worldpay.andrei.offers.persistance.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
@RunWith(MockitoJUnitRunner.class)
public class OfferServiceTest {

    private static final String DESCRIPTION_ONE = "Free next day delivery on orders placed before 5pm";
    private static final String DESCRIPTION_TWO = "Buy 5 for the price of 4!";
    @Mock
    private OfferRepository offerRepository;
    @Mock
    private ProductRepository productRepository;
    @InjectMocks
    private OfferService offerService;
    private Offer offer;
    private Offer offer2;
    private List offers;
    private Product product;


    @Before
    public void setUp() throws Exception {
        reset(offerRepository);
        product = mock(Product.class);
        offer = new Offer(product, DESCRIPTION_ONE, new BigDecimal(10), "GBP");
        offer2 = new Offer(product, DESCRIPTION_TWO, new BigDecimal(40), "GBP");
        offers = new ArrayList<>();
        offers.add(offer);
        offers.add(offer2);
        when(offerRepository.findByProductId(1L)).thenReturn(offers);

    }

    @Test
    public void getAllOffersDelegatesToRepository() throws Exception {
        assertThat(offerService.getAllOffers(1L), is(offers));
        verify(offerRepository).findByProductId(1L);

    }

    @Test
    public void getOfferReturnsOffer() throws Exception {
        when(offerRepository.findByProductIdAndId(1L, 1L)).thenReturn(offer);
        assertThat(offerService.getOffer(1L, 1L), is(offer));
        verify(offerRepository).findByProductIdAndId(1L, 1L);
    }

    @Test (expected = OfferNotFoundException.class)
    public void getOfferThrowsNotFoundExceptionWhenInvalidOfferId() throws Exception {
        when(offerRepository.findByProductIdAndId(1L, 3L)).thenReturn(null);
        offerService.getOffer(1L, 3L);
        verify(offerRepository).findOne(3L);
    }

    @Test
    public void addOfferCreatesOfferAddsToPersistenceAndReturns() throws Exception {
        ArgumentCaptor<Offer> argumentCaptor = ArgumentCaptor.forClass(Offer.class);
        when(offerRepository.save(any(Offer.class))).thenReturn(offer);
        when(productRepository.findOne(1L)).thenReturn(product);
        Offer returned = offerService.addOffer(1L,DESCRIPTION_ONE, "10.00","GBP");
        //returns object returned by persistence layer
        assertThat(returned, is(offer));
        verify(offerRepository).save(argumentCaptor.capture());
        Offer createdOffer = argumentCaptor.getValue();
        //verify we got the product from the product repository
        verify(productRepository).findOne(1L);

        //assert the passed in parameters were added into the new offer
        assertThat(createdOffer.getProduct(), is(product));
        assertThat(createdOffer.getDescription(), is(DESCRIPTION_ONE));
        assertThat(createdOffer.getPriceCurrency(),is("GBP"));
        assertThat(createdOffer.getPrice(), is(equalTo(new BigDecimal("10.00"))));
    }


}