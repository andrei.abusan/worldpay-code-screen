package com.worldpay.andrei.offers.service;

import com.worldpay.andrei.offers.exceptions.ProductNotFoundException;
import com.worldpay.andrei.offers.model.Product;
import com.worldpay.andrei.offers.persistance.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    private static final String DESCRIPTION_ONE = "Russian solution to space writing";
    private static final String PRODUCT_NAME_ONE = "pencil";
    private static final String PRODUCT_NAME_TWO = "space pen";
    private static final String DESCRIPTION_TWO = "American solution to space writing";

    @Mock
    private ProductRepository productRepository;
    @InjectMocks
    private ProductService productService;
    private Product product;
    private Product product2;
    private List<Product> products;

    @Before
    public void setUp() throws Exception {
        product = new Product(PRODUCT_NAME_ONE, DESCRIPTION_ONE);
        product2 = new Product(PRODUCT_NAME_TWO, DESCRIPTION_TWO);
        products = new ArrayList<>();
        products.add(product);
        products.add(product2);
        when(productRepository.findAll()).thenReturn(products);

    }

    @Test
    public void getProductDelegatesToRepository() throws Exception {
        when(productRepository.findOne(1L)).thenReturn(product);
        assertThat(productService.getProduct(1L), is(product));
        verify(productRepository).findOne(1L);
    }

    @Test (expected = ProductNotFoundException.class)
    public void getProductThrowsNotFoundException() throws Exception {
        when(productRepository.findOne(3L)).thenReturn(null);
        productService.getProduct(3L);
        verify(productRepository).findOne(3L);
    }

    @Test
    public void getAllProducts() throws Exception {
        assertThat(productService.getAllProducts(), is(products));
        verify(productRepository).findAll();
    }

}