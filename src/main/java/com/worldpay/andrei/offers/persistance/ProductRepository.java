package com.worldpay.andrei.offers.persistance;

import com.worldpay.andrei.offers.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
