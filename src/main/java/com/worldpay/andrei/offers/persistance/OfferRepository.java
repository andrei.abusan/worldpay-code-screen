package com.worldpay.andrei.offers.persistance;

import com.worldpay.andrei.offers.model.Offer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface OfferRepository extends JpaRepository<Offer, Long> {
    Collection<Offer> findByProductId(long productId);

    Offer findByProductIdAndId(long productId, long offerId);
}
