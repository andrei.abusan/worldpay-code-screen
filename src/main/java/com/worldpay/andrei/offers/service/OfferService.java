package com.worldpay.andrei.offers.service;

import com.worldpay.andrei.offers.exceptions.OfferNotFoundException;
import com.worldpay.andrei.offers.model.Offer;
import com.worldpay.andrei.offers.persistance.OfferRepository;
import com.worldpay.andrei.offers.persistance.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collection;

@Service
public class OfferService {

    public Collection<Offer> getAllOffers(long productId) {
        return offerRepository.findByProductId(productId);
    }

    @Autowired
    private OfferRepository offerRepository;
    @Autowired
    private ProductRepository productRepository;

    public Offer getOffer(long productId, long offerId) {
        Offer offer = offerRepository.findByProductIdAndId(productId, offerId);
        if (offer == null) {
            throw new OfferNotFoundException(productId, offerId);
        }
        return offer;
    }

    public Offer addOffer(long productId, String description, String priceString, String priceCurrency) {
        Offer offer = new Offer(productRepository.findOne(productId), description, new BigDecimal(priceString), priceCurrency);
        return offerRepository.save(offer);

    }
}
