package com.worldpay.andrei.offers.service;

import com.worldpay.andrei.offers.exceptions.ProductNotFoundException;
import com.worldpay.andrei.offers.model.Product;
import com.worldpay.andrei.offers.persistance.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Product getProduct(long productId) {
        Product product = productRepository.findOne(productId);
        if (product == null) {
            throw new ProductNotFoundException(productId);
        }
        return product;
    }

    public Collection<Product> getAllProducts() {
        return productRepository.findAll();
    }
}
