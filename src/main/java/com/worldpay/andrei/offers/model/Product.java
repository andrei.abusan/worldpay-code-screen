package com.worldpay.andrei.offers.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class Product {

    @Id
    @GeneratedValue
    private long id;
    private String productName;
    private String description;
    @OneToMany(mappedBy = "product")
    private Set<Offer> offers;

    public Set<Offer> getOffers() {
        return offers;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", description='" + description + '\'' +
                '}';

    }

    public long getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getDescription() {
        return description;
    }

    public Product() {

    }

    public Product(String productName, String description) {

        this.productName = productName;
        this.description = description;
    }
}
