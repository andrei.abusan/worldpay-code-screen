package com.worldpay.andrei.offers.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

@Entity
public class Offer {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    @JsonIgnore
    private Product product;
    private String description;
    private String priceCurrency;
    private BigDecimal price;

    public Offer() {
    }

    public Offer(Product product, String description, BigDecimal price, String priceCurrency) {

        this.product = product;
        this.description = description;
        this.priceCurrency = priceCurrency;
        this.price = price;
    }

    public long getId() {

        return id;
    }

    public Product getProduct() {
        return product;
    }

    public String getDescription() {
        return description;
    }

    public String getPriceCurrency() {
        return priceCurrency;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
