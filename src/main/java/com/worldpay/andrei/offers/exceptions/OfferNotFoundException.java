package com.worldpay.andrei.offers.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class OfferNotFoundException extends RuntimeException {
    public OfferNotFoundException() {
        super();
    }

    public OfferNotFoundException(long productId, long offerId) {
        super("Could not find offer with ID:'" + offerId + "' associated with product with ID: '" + productId+"'.");
    }
}
