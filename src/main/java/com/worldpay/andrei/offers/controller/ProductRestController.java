package com.worldpay.andrei.offers.controller;

import com.worldpay.andrei.offers.model.Product;
import com.worldpay.andrei.offers.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class ProductRestController {

    @RequestMapping(method = RequestMethod.GET, value = "/products")
    public Collection<Product> getProducts(){
        return productService.getAllProducts();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/products/{productId}")
    public Product getProduct(@PathVariable long productId){
        return productService.getProduct(productId);
    }
    @Autowired
    private ProductService productService;
}
