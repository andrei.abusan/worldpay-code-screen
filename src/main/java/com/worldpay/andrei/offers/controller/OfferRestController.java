package com.worldpay.andrei.offers.controller;


import com.worldpay.andrei.offers.model.Offer;
import com.worldpay.andrei.offers.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController()
public class OfferRestController {

    @Autowired
    private OfferService offerService;

    @RequestMapping(method =  RequestMethod.GET, value = "/products/{productId}/offers")
    public Collection<Offer> getOffers(@PathVariable long productId){
        return offerService.getAllOffers(productId);
    }

    @RequestMapping(method =  RequestMethod.POST, value = "/products/{productId}/offers")
    @ResponseStatus(HttpStatus.CREATED)
    public Offer addOffer(@PathVariable long productId, @RequestParam(name = "description") String description,
    @RequestParam(name = "priceString") String priceString, @RequestParam(name = "priceCurrency") String priceCurrency){
        return offerService.addOffer(productId, description, priceString, priceCurrency);
    }

    @RequestMapping(method =  RequestMethod.GET, value = "/products/{productId}/offers/{offerId}")
    public Offer getOffer(@PathVariable long productId, @PathVariable long offerId){
        return offerService.getOffer(productId, offerId);
    }

}
