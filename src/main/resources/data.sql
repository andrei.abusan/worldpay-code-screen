insert into product(product_name,description) values
('pencil','Russian solution to space writing'),
('space pen','American solution to space writing');
insert into offer(description,price,price_currency,product_id) values
('Free next day delivery on orders placed before 5pm','5.00','GBP',1),
('Buy 5 for the price of 4!','40.00','GBP',1);