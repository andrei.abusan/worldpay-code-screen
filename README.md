#Solution to exercise by Andrei Abusan

##Requirements to run
* Java 8 (tested with openjdk locally)
* Maven 3 (tested with 3.3.9)

##Instructions to run and use
1. Clone repository
2. execute `mvn test`. This will compile the code and tests and execute the tests
3. execute `mvn spring-boot:run`. This will run the rest API at the address `http://localhost:8080` and load it with default
data as described in `/src/main/resources/data.sql`
4. Check that you can run a get command on /products by either hitting `http://localhost:8080/products` in your browser
or running `curl http://localhost:8080/products` from a command line
5. Using the same preferred method send a request to http://localhost:8080/products/1 (or 2) to get a specific product   
6. Using the same preferred method send a request to http://localhost:8080/products/1/offers to get offers associated with product
7. Create a new offer by posting a request to the API with `curl --request POST 'http://localhost:8080/products/2/offers' --data-urlencode "description=some description" --data-urlencode "priceString=10.5" --data-urlencode "priceCurrency=GBP"`
or other preferred method
  
##Improvements that I would prioritize for "next version":
 * Add validation around parameters, most pressing of which:
    * price should be a valid number, greater than 0, with at most 2 decimal point digits
    * restrict what currencies are available with an enum
 * Stop user from creating duplicate entries (IE same description price and currency combination for same product)
 * implement updating and deletion of offers
 * implement creation, updating and deletion of products
 
##Assumptions
* A product can have any number of offers listed against it
* User will only give valid valid parameters when creating offers